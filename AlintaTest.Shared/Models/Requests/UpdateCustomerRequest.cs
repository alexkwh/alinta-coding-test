﻿using System;

namespace AlintaTest.Shared.Models.Requests
{
    public class UpdateCustomerRequest
    {
        public long CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
