﻿using AlintaTest.Infrastructure.Database;
using AlintaTest.Infrastructure.Database.Entities;
using AlintaTest.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AlintaTest.Infrastructure.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly AlintaDbContext _context;
        private readonly ILogger<CustomerRepository> _logger;

        public CustomerRepository(AlintaDbContext context, ILogger<CustomerRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<long> CreateCustomerAsync(Customer customer, CancellationToken token)
        {
            await _context.Customers.AddAsync(customer, token);

            await _context.SaveChangesAsync(token);

            return customer.Id;
        }

        public async Task DeleteCustomerAsync(long id, CancellationToken token)
        {
            var customer = await _context.Customers.FindAsync(new object[] { id }, token);

            if (customer != null)
            {
                _context.Customers.Remove(customer);

                await _context.SaveChangesAsync(token);
            }
        }

        public async Task<Customer> GetCustomerAsync(long id, CancellationToken token)
        {
            var c = await _context.Customers.FindAsync(new object[] { id }, token);
            return c;
        }

        public async Task<Customer> SearchCustomerAsync(string name, CancellationToken token)
        {
            return await _context.Customers.Where(c => c.FirstName.Contains(name) || c.LastName.Contains(name)).FirstOrDefaultAsync();
        }

        public async Task UpdateCustomerAsync(Customer customer, CancellationToken token)
        {
            _context.Customers.Update(customer);

            await _context.SaveChangesAsync(token);
        }
    }
}
