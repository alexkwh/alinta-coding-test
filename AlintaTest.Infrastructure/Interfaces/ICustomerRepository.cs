﻿using AlintaTest.Infrastructure.Database.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace AlintaTest.Infrastructure.Interfaces
{
    public interface ICustomerRepository
    {
        Task<long> CreateCustomerAsync(Customer customer, CancellationToken token);
        Task<Customer> GetCustomerAsync(long id, CancellationToken token);
        Task<Customer> SearchCustomerAsync(string name, CancellationToken token);
        Task UpdateCustomerAsync(Customer customer, CancellationToken token);
        Task DeleteCustomerAsync(long id, CancellationToken token);
    }
}
