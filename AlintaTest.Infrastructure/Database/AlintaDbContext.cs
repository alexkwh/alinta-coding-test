﻿using AlintaTest.Infrastructure.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace AlintaTest.Infrastructure.Database
{
    public class AlintaDbContext : DbContext
    {
        public AlintaDbContext(DbContextOptions<AlintaDbContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}
