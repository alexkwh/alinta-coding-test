﻿using AlintaTest.Core.Interfaces;
using AlintaTest.Shared.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace AlintaTest.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : Controller
    {
        private readonly ICustomerService _customerService;
        private readonly ILogger<CustomersController> _logger;

        public CustomersController(ICustomerService customerService, ILogger<CustomersController> logger)
        {
            _customerService = customerService;
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> CreateCustomer(CreateCustomerRequest request, CancellationToken token)
        {
            _logger.LogDebug("Received request to create customer");

            var customerId = await _customerService.CreateCustomerAsync(request, token);

            return Ok(customerId);
        }

        [HttpGet]
        [ProducesResponseType(typeof(SearchCustomerResponse), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> SearchCustomer(string name, CancellationToken token)
        {
            _logger.LogDebug($"Received request to search for customer with name: {name}");

            var customer = await _customerService.SearchCustomerAsync(name, token);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateCustomer(UpdateCustomerRequest request, CancellationToken token)
        {
            _logger.LogDebug($"Received request to update for customer with id: {request.CustomerId}");

            await _customerService.UpdateCustomerAsync(request, token);

            return Ok();
        }

        [HttpDelete("{id:long}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DeleteCustomer(long id, CancellationToken token)
        {
            _logger.LogDebug($"Received request to delete for customer with id: {id}");

            await _customerService.DeleteCustomerAsync(id, token);

            return Ok();
        }
    }
}
