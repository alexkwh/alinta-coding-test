﻿using AlintaTest.Infrastructure.Database;
using AlintaTest.Infrastructure.Database.Entities;
using AlintaTest.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AlintaTest.Tests.RepositoryTests
{
    public class CustomerRepositoryTests
    {
        private readonly AlintaDbContext _context;
        private readonly Mock<ILogger<CustomerRepository>> _mockLogger;
        private CustomerRepository _customerRepository;

        public CustomerRepositoryTests()
        {
            var dbOptions = new DbContextOptionsBuilder<AlintaDbContext>().UseInMemoryDatabase("AlintaTestDB").Options;
            _context = new AlintaDbContext(dbOptions);

            _mockLogger = new Mock<ILogger<CustomerRepository>>();

            _customerRepository = new CustomerRepository(_context, _mockLogger.Object);
        }

        [Fact]
        public async Task CreateCustomerAsync_Should_Return_CustomerId_When_CreateSuccess()
        {
            var newCustomer = new Customer 
            { 
                FirstName = "FirstName",
                LastName = "LastName",
                DateOfBirth = DateTime.UtcNow.AddDays(-18)
            };

            var cancellationToken = new CancellationToken();

            var customerId = await _customerRepository.CreateCustomerAsync(newCustomer, cancellationToken);

            var createdCustomer = await _context.Customers.FindAsync(new object[] { customerId }, cancellationToken);

            Assert.NotNull(createdCustomer);
            Assert.Equal(newCustomer.Id, createdCustomer.Id);
        }
    }
}
