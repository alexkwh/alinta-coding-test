﻿using AlintaTest.Core.Services;
using AlintaTest.Infrastructure.Database.Entities;
using AlintaTest.Infrastructure.Interfaces;
using AlintaTest.Shared.Models.Requests;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AlintaTest.Tests.ServiceTests
{
    public class CustomerServiceTests
    {
        private readonly Mock<ICustomerRepository> _mockCustomerRepository;
        private readonly Mock<ILogger<CustomerService>> _mockLogger;
        private CustomerService _customerService;

        public CustomerServiceTests()
        {
            _mockCustomerRepository = new Mock<ICustomerRepository>();
            _mockLogger = new Mock<ILogger<CustomerService>>();

            _customerService = new CustomerService(_mockCustomerRepository.Object, _mockLogger.Object);
        }

        [Fact]
        public async Task CreateCustomerAsync_Should_Return_CustomerId_When_CreateSuccess()
        {
            _mockCustomerRepository.Setup(x => x.CreateCustomerAsync(It.IsAny<Customer>(), It.IsAny<CancellationToken>())).ReturnsAsync(1);

            var request = new CreateCustomerRequest
            {
                FirstName = "",
                LastName = "",
                DateOfBirth = DateTime.UtcNow.AddDays(-18)
            };

            var customerId = await _customerService.CreateCustomerAsync(request, new CancellationToken());

            Assert.Equal(1, customerId);
        }

        [Fact]
        public async Task SearchCustomerAsync_Should_Return_Null_When_NoCustomerFound()
        {
            _mockCustomerRepository.Setup(x => x.SearchCustomerAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).Verifiable();

            var customer = await _customerService.SearchCustomerAsync(null, new CancellationToken());

            _mockCustomerRepository.Verify(x => x.SearchCustomerAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()));

            Assert.Null(customer);
        }

        [Fact]
        public async Task SearchCustomerAsync_Should_Return_Customer_When_CustomerFound()
        {
            var customer = new Customer
            {
                Id = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                DateOfBirth = DateTime.UtcNow.AddDays(-18)
            };

            _mockCustomerRepository.Setup(x => x.SearchCustomerAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(customer);

            var response = await _customerService.SearchCustomerAsync("FirstName", new CancellationToken());

            Assert.NotNull(response);
            Assert.Equal(1, response.CustomerId);
            Assert.Equal("FirstName", response.FirstName);
            Assert.Equal("LastName", response.LastName);
        }

        [Fact]
        public async Task UpdateCustomerAsync_Should_Return_Null_When_NoCustomerFound()
        {
            _mockCustomerRepository.Setup(x => x.GetCustomerAsync(It.IsAny<long>(), It.IsAny<CancellationToken>()));

            var request = new UpdateCustomerRequest
            {
                CustomerId = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                DateOfBirth = DateTime.UtcNow.AddDays(-18)
            };

            await _customerService.UpdateCustomerAsync(request, new CancellationToken());

            _mockCustomerRepository.Verify(x => x.GetCustomerAsync(It.IsAny<long>(), It.IsAny<CancellationToken>()));
            _mockCustomerRepository.Verify(x => x.UpdateCustomerAsync(It.IsAny<Customer>(), It.IsAny<CancellationToken>()), Times.Never);
        }

        [Fact]
        public async Task UpdateCustomerAsync_Should_Return_Ok_When_CustomerFound()
        {
            _mockCustomerRepository.Setup(x => x.GetCustomerAsync(It.IsAny<long>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Customer { });

            var request = new UpdateCustomerRequest
            {
                CustomerId = 1,
                FirstName = "FirstName",
                LastName = "LastName",
                DateOfBirth = DateTime.UtcNow.AddDays(-18)
            };

            await _customerService.UpdateCustomerAsync(request, new CancellationToken());

            _mockCustomerRepository.Verify(x => x.GetCustomerAsync(It.IsAny<long>(), It.IsAny<CancellationToken>()));
            _mockCustomerRepository.Verify(x => x.UpdateCustomerAsync(It.IsAny<Customer>(), It.IsAny<CancellationToken>()));
        }

        [Fact]
        public async Task DeleteCustomerAsync_Should_Return_Ok_When_DeleteSuccess()
        {
            _mockCustomerRepository.Setup(x => x.DeleteCustomerAsync(It.IsAny<long>(), It.IsAny<CancellationToken>())).Verifiable();

            await _customerService.DeleteCustomerAsync(1, new CancellationToken());

            _mockCustomerRepository.Verify(x => x.DeleteCustomerAsync(It.IsAny<long>(), It.IsAny<CancellationToken>()));
        }
    }
}
