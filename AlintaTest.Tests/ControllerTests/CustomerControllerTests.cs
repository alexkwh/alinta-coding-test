using AlintaTest.API.Controllers;
using AlintaTest.Core.Interfaces;
using AlintaTest.Shared.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AlintaTest.Tests.ControllerTests
{
    public class CustomerControllerTests
    {
        private readonly Mock<ICustomerService> _mockCustomerService;
        private readonly Mock<ILogger<CustomersController>> _mockLogger;
        private CustomersController _customersController;

        public CustomerControllerTests()
        {
            _mockCustomerService = new Mock<ICustomerService>();
            _mockLogger = new Mock<ILogger<CustomersController>>();

            _customersController = new CustomersController(_mockCustomerService.Object, _mockLogger.Object);
        }

        [Fact]
        public async Task CreateCustomer_Should_Return_Ok()
        {
            _mockCustomerService.Setup(x => x.CreateCustomerAsync(It.IsAny<CreateCustomerRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(1);

            var request = new CreateCustomerRequest
            { 
                FirstName = "FirstName",
                LastName = "LastName",
                DateOfBirth = DateTime.UtcNow.AddDays(-18)
            };

            var result = await _customersController.CreateCustomer(request, new CancellationToken());

            Assert.IsAssignableFrom<IActionResult>(result);

            var okResult = result as OkObjectResult;
            Assert.NotNull(okResult);
        }
    }
}
