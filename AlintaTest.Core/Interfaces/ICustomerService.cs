﻿using AlintaTest.Shared.Models.Requests;
using System.Threading;
using System.Threading.Tasks;

namespace AlintaTest.Core.Interfaces
{
    public interface ICustomerService
    {
        Task<long> CreateCustomerAsync(CreateCustomerRequest createCustomerRequest, CancellationToken token);
        Task<SearchCustomerResponse> SearchCustomerAsync(string name, CancellationToken token);
        Task UpdateCustomerAsync(UpdateCustomerRequest updateCustomerRequest, CancellationToken token);
        Task DeleteCustomerAsync(long id, CancellationToken token);
    }
}
