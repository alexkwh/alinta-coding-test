﻿using AlintaTest.Core.Interfaces;
using AlintaTest.Infrastructure.Database.Entities;
using AlintaTest.Infrastructure.Interfaces;
using AlintaTest.Shared.Models.Requests;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace AlintaTest.Core.Services
{
    public class CustomerService : ICustomerService
    {
        public readonly ICustomerRepository _customerRepository;
        private readonly ILogger<CustomerService> _logger;

        public CustomerService(ICustomerRepository customerRepository, ILogger<CustomerService> logger)
        {
            _customerRepository = customerRepository;
            _logger = logger;
        }

        public async Task<long> CreateCustomerAsync(CreateCustomerRequest request, CancellationToken token)
        {
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                DateOfBirth = request.DateOfBirth
            };

            return await _customerRepository.CreateCustomerAsync(customer, token);
        }

        public async Task DeleteCustomerAsync(long id, CancellationToken token)
        {
            await _customerRepository.DeleteCustomerAsync(id, token);
        }

        public async Task<SearchCustomerResponse> SearchCustomerAsync(string name, CancellationToken token)
        {
            var customer = await _customerRepository.SearchCustomerAsync(name, token);

            if (customer == null)
            {
                return null;
            }

            return new SearchCustomerResponse 
            { 
                CustomerId = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                DateOfBirth= customer.DateOfBirth
            };
        }

        public async Task UpdateCustomerAsync(UpdateCustomerRequest request, CancellationToken token)
        {
            var customer = await _customerRepository.GetCustomerAsync(request.CustomerId, token);

            if (customer != null)
            { 
                customer.FirstName = request.FirstName;
                customer.LastName = request.LastName;
                customer.DateOfBirth = request.DateOfBirth;

                await _customerRepository.UpdateCustomerAsync(customer, token);
            }
        }
    }
}
